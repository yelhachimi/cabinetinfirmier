package fr.cabinetinfirmier.repositories;

import fr.cabinetinfirmier.models.Infirmiere;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfirmiereRepository extends JpaRepository<Infirmiere, Integer> {
}
