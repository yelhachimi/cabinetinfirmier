package fr.cabinetinfirmier.repositories;

import fr.cabinetinfirmier.models.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<Adresse, Integer> {
}
