package fr.cabinetinfirmier.repositories;

import fr.cabinetinfirmier.models.Deplacement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeplacementRepository extends JpaRepository<Deplacement, Integer> {

}
