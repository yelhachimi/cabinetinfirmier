package fr.cabinetinfirmier.repositories;

import fr.cabinetinfirmier.models.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
}
