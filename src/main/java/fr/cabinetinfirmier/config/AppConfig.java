package fr.cabinetinfirmier.config;

import fr.cabinetinfirmier.repositories.AdresseRepository;
import fr.cabinetinfirmier.repositories.DeplacementRepository;
import fr.cabinetinfirmier.repositories.InfirmiereRepository;
import fr.cabinetinfirmier.repositories.PatientRepository;
import fr.cabinetinfirmier.services.AdresseService;
import fr.cabinetinfirmier.services.DeplacementService;
import fr.cabinetinfirmier.services.InfirmiereService;
import fr.cabinetinfirmier.services.PatientService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public AdresseService adresseService(AdresseRepository adresseRepository)
    {
        return new AdresseService(adresseRepository);
    }

    @Bean
    public InfirmiereService infirmiereService(InfirmiereRepository infirmiereRepository)
    {
        return new InfirmiereService(infirmiereRepository);
    }

    @Bean
    public PatientService patientService(PatientRepository patientRepository)
    {
        return new PatientService(patientRepository);
    }

    @Bean
    public DeplacementService deplacementService(DeplacementRepository deplacementRepository)
    {
        return new DeplacementService(deplacementRepository);
    }
}
