package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Infirmiere;

import java.util.List;

public interface InfirmiereServiceInter {
    List<Infirmiere> getInfirmieres();

    Infirmiere getInfirmiereById(Integer id);

    Infirmiere saveInfimiere(Infirmiere infirmiere);

    Infirmiere updateInfirmiere(Infirmiere infirmiere);

    String deleteInfimiere(Integer id);
}
