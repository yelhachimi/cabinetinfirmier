package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Patient;

import java.util.List;

public interface PatientServiceInter {
    List<Patient> getPatients();

    Patient getPatientById(Integer id);

    Patient savePatient(Patient patient);

    Patient updatePatient(Patient patient);

    String deletePatient(Integer id);
}
