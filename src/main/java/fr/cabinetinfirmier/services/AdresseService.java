package fr.cabinetinfirmier.services;


import fr.cabinetinfirmier.models.Adresse;
import fr.cabinetinfirmier.repositories.AdresseRepository;

import java.util.List;

public class AdresseService implements AdresseServiceInter {

    private AdresseRepository adresseRepository;

    public AdresseService(AdresseRepository adresseRepository)
    {
        this.adresseRepository = adresseRepository;
    }

    @Override
    public List<Adresse> getAdresses()
    {
        return this.adresseRepository.findAll();
    }

    @Override
    public Adresse getAdresseById(Integer id)
    {
        return this.adresseRepository.findById(id).get();
    }

    @Override
    public Adresse saveAdresse(Adresse adresse)
    {
       return this.adresseRepository.save(adresse);
    }

    @Override
    public Adresse updateAdresse(Adresse adresse)
    {
        return this.adresseRepository.save(adresse);
    }

    @Override
    public String deleteAdresse(Integer id)
    {
        this.adresseRepository.deleteById(id);
        return "Adresse supprimée.";
    }
}
