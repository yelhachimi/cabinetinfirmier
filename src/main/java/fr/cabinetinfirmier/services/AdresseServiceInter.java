package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Adresse;

import java.util.List;

public interface AdresseServiceInter {
    List<Adresse> getAdresses();

    Adresse getAdresseById(Integer id);

    Adresse saveAdresse(Adresse adresse);

    Adresse updateAdresse(Adresse adresse);

    String deleteAdresse(Integer id);
}
