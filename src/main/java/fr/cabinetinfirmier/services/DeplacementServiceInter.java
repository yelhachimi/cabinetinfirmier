package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Deplacement;

import java.util.List;

public interface DeplacementServiceInter {
    List<Deplacement> getDeplacements();

    Deplacement getDeplacementById(Integer id);

    Deplacement saveDeplacement(Deplacement deplacement);

    Deplacement updateDeplacement(Deplacement deplacement);

    String deleteDeplacement(Integer id);
}
