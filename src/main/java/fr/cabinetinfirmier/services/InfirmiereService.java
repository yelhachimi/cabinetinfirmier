package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Adresse;
import fr.cabinetinfirmier.models.Infirmiere;
import fr.cabinetinfirmier.repositories.AdresseRepository;
import fr.cabinetinfirmier.repositories.InfirmiereRepository;

import java.util.List;

public class InfirmiereService implements InfirmiereServiceInter {

    private InfirmiereRepository infirmiereRepository;

    public InfirmiereService(InfirmiereRepository infirmiereRepository)
    {
        this.infirmiereRepository = infirmiereRepository;
    }

    @Override
    public List<Infirmiere> getInfirmieres()
    {
        return this.infirmiereRepository.findAll();
    }

    @Override
    public Infirmiere getInfirmiereById(Integer id)
    {
        return this.infirmiereRepository.findById(id).get();
    }

    @Override
    public Infirmiere saveInfimiere(Infirmiere infirmiere)
    {
        return this.infirmiereRepository.save(infirmiere);
    }

    @Override
    public Infirmiere updateInfirmiere(Infirmiere infirmiere)
    {
        return this.infirmiereRepository.save(infirmiere);
    }

    @Override
    public String deleteInfimiere(Integer id)
    {
        this.infirmiereRepository.deleteById(id);
        return "Infirmière morte. RIP";
    }
}
