package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Deplacement;
import fr.cabinetinfirmier.repositories.DeplacementRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class DeplacementService implements DeplacementServiceInter {

    private DeplacementRepository deplacementRepository;

    public DeplacementService(DeplacementRepository deplacementRepository)
    {
        this.deplacementRepository = deplacementRepository;
    }

    @Override
    public List<Deplacement> getDeplacements()
    {
        return this.deplacementRepository.findAll();
    }

    @Override
    public Deplacement getDeplacementById(Integer id)
    {
        return this.deplacementRepository.findById(id).get();
    }

    @Override
    public Deplacement saveDeplacement(Deplacement deplacement)
    {
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        format.format(deplacement.getDate());
        return this.deplacementRepository.save(deplacement);
    }

    @Override
    public Deplacement updateDeplacement(Deplacement deplacement)
    {
        return this.deplacementRepository.save(deplacement);
    }

    @Override
    public String deleteDeplacement(Integer id)
    {
        this.deplacementRepository.deleteById(id);
        return "Deplacement supprimé.";
    }
}
