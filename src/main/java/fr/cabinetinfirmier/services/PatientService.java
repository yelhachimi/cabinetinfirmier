package fr.cabinetinfirmier.services;

import fr.cabinetinfirmier.models.Patient;
import fr.cabinetinfirmier.repositories.PatientRepository;

import java.util.List;

public class PatientService implements PatientServiceInter {

    private PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository)
    {
        this.patientRepository = patientRepository;
    }

    @Override
    public List<Patient> getPatients()
    {
        return this.patientRepository.findAll();
    }

    @Override
    public Patient getPatientById(Integer id)
    {
        return this.patientRepository.findById(id).get();
    }

    @Override
    public Patient savePatient(Patient patient)
    {
        return this.patientRepository.save(patient);
    }

    @Override
    public Patient updatePatient(Patient patient)
    {
        return this.patientRepository.save(patient);
    }

    @Override
    public String deletePatient(Integer id)
    {
        this.patientRepository.deleteById(id);
        return "Le patient est mort ...";
    }
}
