package fr.cabinetinfirmier.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="deplacement")
public class Deplacement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    private Patient patient;

    @Column(name = "date")
    private Date date;

    @Column(name = "cout")
    private Double cout;

    @ManyToOne
    private Infirmiere infirmiere;
}
