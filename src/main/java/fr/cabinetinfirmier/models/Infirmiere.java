package fr.cabinetinfirmier.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="infirmiere")
public class Infirmiere {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @ManyToOne
    private Adresse adresse;

    @Column(name = "numero_professionnel")
    private Integer numero_professionnel;

    @Column(name = "nom")
    private String nom;

    @Column(name="prenom")
    private String prenom;

    @Column(name = "tel_pro")
    private String tel_pro;

    @Column(name = "tel_perso")
    private String tel_perso;

//    @OneToMany(targetEntity = Patient.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "infirmiere_id", referencedColumnName = "id")
//    private Set<Patient> patients;

//    @OneToMany(targetEntity = Deplacement.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "infirmiere_id", referencedColumnName = "id")
//    private Set<Deplacement> deplacements;
}
