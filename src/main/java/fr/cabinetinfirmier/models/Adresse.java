package fr.cabinetinfirmier.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="adresse", uniqueConstraints = @UniqueConstraint(name = "uk_adresse_constraint",
        columnNames = {"numero", "rue"}))

public class Adresse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="numero")
    private String numero;

    @Column(name = "rue")
    private String rue;

    @Column(name = "cp")
    private String cp;

    @Column(name = "ville")
    private String ville;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "adresse_id", referencedColumnName = "id")
//    private Set<Infirmiere> infirmieres;

//    @OneToMany(cascade = CascadeType.ALL)
//    // @JoinColumn(name = "adresse_id", referencedColumnName = "id")
//    private Set<Patient> patients;

}
