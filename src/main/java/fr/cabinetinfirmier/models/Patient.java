package fr.cabinetinfirmier.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="patient")
public class Patient {

    public enum Sexe{
        HOMME,
        FEMME,
        HELICOPTERE,
        CASSEROLE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @ManyToOne
    private Adresse adresse;

    @ManyToOne
    private Infirmiere infirmiere;

    @Column(name="nom")
    private String nom;

    @Column(name ="prenom")
    private String prenom;

    @Column(name="date_de_naissance")
    private Date date_de_naissance;

    @Column(columnDefinition = "ENUM('HOMME', 'FEMME', 'HELICOPTERE', 'CASSEROLE')")
    @Enumerated(EnumType.STRING)
    private Sexe sexe;

    @Column(name="numero_securite_sociale")
    private Integer numero_securite_sociale;

//    @OneToMany(targetEntity = Deplacement.class, cascade = CascadeType.ALL)
//    @JoinColumn(name = "patient_id", referencedColumnName = "id")
//    private Set<Deplacement> deplacements;

}
