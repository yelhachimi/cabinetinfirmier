package fr.cabinetinfirmier.controllers;

import fr.cabinetinfirmier.models.Adresse;
import fr.cabinetinfirmier.services.AdresseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/adresses")
public class AdresseController {

    Logger LOGGER = LoggerFactory.getLogger(AdresseController.class);
    private AdresseService adresseService;

    public AdresseController(AdresseService adresseService)
    {
        this.adresseService = adresseService;
    }

    @GetMapping
    public List<Adresse> getAdresses()
    {
        LOGGER.info("Récupération de toutes les adresses via le service");
        return this.adresseService.getAdresses();
    }

    @GetMapping("{id}")
    public Adresse getAdresseById(@PathVariable Integer id)
    {
        LOGGER.info("Récupération d'une adresse via son id");
        return this.adresseService.getAdresseById(id);
    }

    @PostMapping
    public Adresse saveAdresse(@RequestBody Adresse adresse)
    {
        LOGGER.info("Sauvegarde d'une adresse dans la bdd via le service");
        return this.adresseService.saveAdresse(adresse);
    }

    @PutMapping
    public Adresse updateAdresse(@RequestBody Adresse adresse)
    {
        LOGGER.info("Modification d'une adresse avec la methode save du service");
        return this.adresseService.updateAdresse(adresse);
    }

    @DeleteMapping("{id}")
    public String deleteAdresse(@PathVariable Integer id)
    {
        LOGGER.info("Suppression d'une adresse via l'id dans la bdd");
        return this.adresseService.deleteAdresse(id);
    }

}
