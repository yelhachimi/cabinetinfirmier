package fr.cabinetinfirmier.controllers;

import fr.cabinetinfirmier.models.Patient;
import fr.cabinetinfirmier.services.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/patients")
public class PatientController {

    Logger LOGGER = LoggerFactory.getLogger(PatientController.class);
    private PatientService patientService;

    public PatientController(PatientService patientService)
    {
        this.patientService = patientService;
    }

    @GetMapping
    public List<Patient> getPatients()
    {
        LOGGER.info("Récupération de tous les patients via le service");
        return this.patientService.getPatients();
    }

    @PostMapping
    public Patient savePatient(@RequestBody Patient patient)
    {
        LOGGER.info("Enregistrement d'un patient dans la bdd via le service");
        return this.patientService.savePatient(patient);
    }

    @GetMapping("{id}")
    public Patient getPatientById(@PathVariable Integer id)
    {
        LOGGER.info("Récupération d'un patient avec son id");
        return this.patientService.getPatientById(id);
    }

    @PutMapping
    public Patient updatePatient(@RequestBody Patient patient)
    {
        LOGGER.info("Mise à jour des informations du patient");
        return this.patientService.updatePatient(patient);
    }

    @DeleteMapping("{id}")
    public String deletePatient(@PathVariable Integer id)
    {
        LOGGER.info("Suppression du patient grace à son id");
        return this.patientService.deletePatient(id);
    }
}
