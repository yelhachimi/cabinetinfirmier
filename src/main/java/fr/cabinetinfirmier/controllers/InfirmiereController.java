package fr.cabinetinfirmier.controllers;

import fr.cabinetinfirmier.models.Infirmiere;
import fr.cabinetinfirmier.services.InfirmiereService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/infirmieres")
public class InfirmiereController {

    Logger LOGGER = LoggerFactory.getLogger(InfirmiereController.class);
    private InfirmiereService infirmiereService;

    public InfirmiereController(InfirmiereService infirmiereService)
    {
        this.infirmiereService = infirmiereService;
    }

    @GetMapping
    public List<Infirmiere> getInfirmieres()
    {
        LOGGER.info("Récupération de toutes les infirmieres via le service");
        return this.infirmiereService.getInfirmieres();
    }

    @GetMapping("{id}")
    public Infirmiere getInfirmiereById(@PathVariable Integer id)
    {
        LOGGER.info("Récupération d'une seule infirmière via son id");
        return this.infirmiereService.getInfirmiereById(id);
    }

    @PostMapping
    public Infirmiere saveInfirmiere(@RequestBody Infirmiere infirmiere)
    {
        LOGGER.info("Sauvegarde d'une infirmière via le service dans la bdd");
        return this.infirmiereService.saveInfimiere(infirmiere);
    }

    @PutMapping
    public Infirmiere updateInfirmiere(@RequestBody Infirmiere infirmiere)
    {
        LOGGER.info("Mise à jour d'une infirmière");
        return this.infirmiereService.updateInfirmiere(infirmiere);
    }

    @DeleteMapping("{id}")
    public String deleteInfirmiere(@PathVariable Integer id)
    {
        LOGGER.info("Suppression d'une infirmière via son id");
        return this.infirmiereService.deleteInfimiere(id);
    }
}
