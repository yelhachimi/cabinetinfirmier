package fr.cabinetinfirmier.controllers;

import fr.cabinetinfirmier.models.Deplacement;
import fr.cabinetinfirmier.services.DeplacementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/deplacements")
public class DeplacementController {

    Logger LOGGER = LoggerFactory.getLogger(DeplacementController.class);
    private DeplacementService deplacementService;

    public DeplacementController(DeplacementService deplacementService)
    {
        this.deplacementService = deplacementService;
    }

    @GetMapping
    public List<Deplacement> getDeplacements()
    {
        LOGGER.info("Récupération de tous les deplacements via le service");
        return this.deplacementService.getDeplacements();
    }

    @PostMapping
    public Deplacement saveDeplacement(@RequestBody Deplacement deplacement)
    {
        LOGGER.info("Sauvegarde d'un déplacement dans la bdd");
        return this.deplacementService.saveDeplacement(deplacement);
    }

    @PutMapping
    public Deplacement updateDeplacement(@RequestBody Deplacement deplacement)
    {
        LOGGER.info("MAJ d'un déplacement");
        return this.deplacementService.updateDeplacement(deplacement);
    }

    @GetMapping("{id}")
    public Deplacement getDeplacementById(@PathVariable Integer id)
    {
        LOGGER.info("Récupération d'un déplacement via son id");
        return this.deplacementService.getDeplacementById(id);
    }

    @DeleteMapping("{id}")
    public String deleteDeplacement(@PathVariable Integer id)
    {
        LOGGER.info("Suppression du déplacement avec son id");
        return this.deplacementService.deleteDeplacement(id);
    }
}
